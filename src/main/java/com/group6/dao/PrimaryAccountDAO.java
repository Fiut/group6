package com.group6.dao;

import org.springframework.data.repository.CrudRepository;

import com.group6.domain.PrimaryAccount;


public interface PrimaryAccountDAO extends CrudRepository<PrimaryAccount,Long> {

    PrimaryAccount findByAccountNumber (int accountNumber);  //find the primaryAccount through the accountNumber
}

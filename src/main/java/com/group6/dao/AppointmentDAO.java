package com.group6.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.group6.domain.Appointment;

public interface AppointmentDAO extends CrudRepository<Appointment, Long> {

    List<Appointment> findAll();
}

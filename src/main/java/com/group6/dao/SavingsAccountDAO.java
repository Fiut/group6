package com.group6.dao;

import org.springframework.data.repository.CrudRepository;

import com.group6.domain.SavingsAccount;

public interface SavingsAccountDAO extends CrudRepository<SavingsAccount, Long> {

    SavingsAccount findByAccountNumber (int accountNumber); //find the savingsaccount through the accountNumber
}

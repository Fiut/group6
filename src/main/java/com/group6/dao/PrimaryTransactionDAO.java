package com.group6.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.group6.domain.PrimaryTransaction;

public interface PrimaryTransactionDAO extends CrudRepository<PrimaryTransaction, Long> {

    List<PrimaryTransaction> findAll(); //will be returning the list of primaryTransactions.
}

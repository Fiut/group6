package com.group6.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.group6.domain.SavingsTransaction;

public interface SavingsTransactionDAO extends CrudRepository<SavingsTransaction, Long> {

    List<SavingsTransaction> findAll();
}


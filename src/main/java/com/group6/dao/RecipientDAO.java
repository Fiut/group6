package com.group6.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.group6.domain.Recipient;

public interface RecipientDAO extends CrudRepository<Recipient, Long> {
    List<Recipient> findAll();

    Recipient findByName(String recipientName);

    void deleteByName(String recipientName);
}

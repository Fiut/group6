package com.group6.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.group6.domain.User;

public interface UserDAO extends CrudRepository<User, Long> { //CrudRepository provides sophisticated CRUD functionality for the entity class that is being managed.
	
	//we want to use the String to retrieve instance of User entity.
	User findByUsername(String username);
	User findByEmail(String email);
	
	List<User> findAll();
}

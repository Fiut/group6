package com.group6.dao;

import org.springframework.data.repository.CrudRepository;

import com.group6.domain.security.Role;

public interface RoleDAO extends CrudRepository<Role, Integer> {
    Role findByName(String name);
}

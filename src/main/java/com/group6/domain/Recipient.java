package com.group6.domain;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity //This class will be considered as entity, this entity will be persisted into the database, in order to do that we must specify a primary key.
public class Recipient {

	@Id //States that the variable 'id' will be the primary key.
	@GeneratedValue(strategy = GenerationType.AUTO) //id will be automatically generated 
	private Long id;
	private String name;
	private String email;
	private String phone;
	private String accountNumber;
	private String description;

	@ManyToOne //Refers to the relationship between Recipient and User. One User can have many Recipient.
	@JoinColumn(name = "user_id") //in order to reference to User, we add a column, which references to the primary key, thus user_id.
	@JsonIgnore //breaks the infinite loop.
	private User user;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}

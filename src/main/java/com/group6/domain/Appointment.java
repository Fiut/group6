package com.group6.domain;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity //This class will be considered as entity, this entity will be persisted into the database, in order to do that we must specify a primary key.
public class Appointment {

	@Id //States that the variable 'id' will be the primary key.
	@GeneratedValue(strategy = GenerationType.AUTO) //id will be automatically generated 
	private Long id;
	private Date date;
	private String location;
	private String description;
	private boolean confirmed; //states whether the appointment has been confirmed.

	@ManyToOne //Refers to the relationship between Appointments and User. One user can have many appointments.
	@JoinColumn(name = "user_id") //in order to reference to User, we add a column, which references to the primary key, thus user_id.
	private User user;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public boolean isConfirmed() {
		return confirmed;
	}

	public void setConfirmed(boolean confirmed) {
		this.confirmed = confirmed;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}

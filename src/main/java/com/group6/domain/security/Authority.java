package com.group6.domain.security;

import org.springframework.security.core.GrantedAuthority;

@SuppressWarnings("serial")
public class Authority implements GrantedAuthority{ //represents an authority granted to an Authentication object.
	//we are defining an authority, with a string authority to reference the authority content, and we are overriding the method getAuthority to return a authority string. 
	    private final String authority;

	    public Authority(String authority) {
	        this.authority = authority;
	    }

	    @Override
	    public String getAuthority() {
	        return authority;
	    }
	}
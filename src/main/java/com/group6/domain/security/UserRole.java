package com.group6.domain.security;

import javax.persistence.*;

import com.group6.domain.User;

@Entity //used to persists our userRole class into the database.
@Table(name="user_role") //using this annotation to specify the name we want it to have in the table.
public class UserRole {
    @Id //specify userRoleId is the set as primary key.
    @GeneratedValue(strategy = GenerationType.AUTO) //the primary key will be automatically generated.
    private long userRoleId;

    public UserRole(User user, Role role) {
        this.user = user;
        this.role = role;
    }


    @ManyToOne(fetch = FetchType.EAGER) //Eager: the class will actively fetch whatever values, when this class is being initiated.
    @JoinColumn(name = "user_id")
    private User user;



    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "role_id")
    private Role role;

    public UserRole() {}

    public long getUserRoleId() {
        return userRoleId;
    }

    public void setUserRoleId(long userRoleId) {
        this.userRoleId = userRoleId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }


}

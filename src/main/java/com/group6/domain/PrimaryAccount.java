package com.group6.domain;


import java.math.BigDecimal;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity //This class will be considered as entity, this entity will be persisted into the database, in order to do that we must specify a primary key.
public class PrimaryAccount {

	@Id //States that the variable 'id' will be the primary key.
	@GeneratedValue(strategy = GenerationType.AUTO) //id will be automatically generated 
	private Long id;
	private int accountNumber;
	private BigDecimal accountBalance; //BigDecimal is a class that represents a floating number, we use this over 'double' because it will double will give an error.

	@OneToMany(mappedBy = "primaryAccount", cascade = CascadeType.ALL, fetch = FetchType.LAZY) //mapping the primaryAccount to the primaryTransactionList. One primaryAccount have have multiple primaryTransactionList. they will be linked by "mappedBy", CascadeType.ALL states that the updates here will happen in primaryAccount.  
	@JsonIgnore //breaks the infinite loop.
	private List<PrimaryTransaction> primaryTransactionList;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(int accountNumber) {
		this.accountNumber = accountNumber;
	}

	public BigDecimal getAccountBalance() {
		return accountBalance;
	}

	public void setAccountBalance(BigDecimal accountBalance) {
		this.accountBalance = accountBalance;
	}

	public List<PrimaryTransaction> getPrimaryTransactionList() {
		return primaryTransactionList;
	}

	public void setPrimaryTransactionList(List<PrimaryTransaction> primaryTransactionList) {
		this.primaryTransactionList = primaryTransactionList;
	}

}

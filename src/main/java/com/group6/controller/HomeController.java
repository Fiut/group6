package com.group6.controller;

import java.security.Principal;
import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.group6.dao.RoleDAO;
import com.group6.domain.PrimaryAccount;
import com.group6.domain.SavingsAccount;
import com.group6.domain.User;
import com.group6.domain.security.UserRole;
import com.group6.service.UserService;

@Controller //registers this class as a controller and a bean in the spring container automatically. 
public class HomeController {

	@Autowired
	private UserService userService;

	@Autowired
	private RoleDAO roleDao;

	@RequestMapping("/") //registering the path mapping for the root path to the 'home' method below. it will deliver a redirect to '/index'
	public String home() {
		return "redirect:/index";
	}

	@RequestMapping("/index") //either we hit the root or the /index, they will both return 'index' as string, which in turn will open the index.html file. This is handled automatically because he have included the appropriate dependence 'thymelee'.
	public String index() {
		return "index";
	}

	@RequestMapping(value = "/signup", method = RequestMethod.GET) //mapping a request to the path '/signup', and it will be a GET method.
	public String signup(Model model) { //we define a model, we bind the user object we have initialised to the user variable in model and then return to the signup.html
		User user = new User();

		model.addAttribute("user", user); //user variable references to a user object.

		return "signup";
	}

	@RequestMapping(value = "/signup", method = RequestMethod.POST) 
	public String signupPost(@ModelAttribute("user") User user, Model model) { //modelAttribute retrieves a variable called user from the context of the html form we have submitted.

		if (userService.checkUserExists(user.getUsername(), user.getEmail())) { //we are invoking the user services that we have autowired here. Passes username and email to check if the user exists.
			//checks them each separately 
			if (userService.checkEmailExists(user.getEmail())) {
				model.addAttribute("emailExists", true);
			}

			if (userService.checkUsernameExists(user.getUsername())) {
				model.addAttribute("usernameExists", true);
			}

			return "signup"; //returns to the signup.html page, if they exist then a text will state this to the user.
		} else {
			Set<UserRole> userRoles = new HashSet<>();
			userRoles.add(new UserRole(user, roleDao.findByName("ROLE_USER")));

			userService.createUser(user, userRoles);

			return "redirect:/";
		}
	}

	@RequestMapping("/userFront")
	public String userFront(Principal principal, Model model) { //takes two parameters, principal is the person who has logged in.
		User user = userService.findByUsername(principal.getName()); //retrieve the user who has logged into the system.
		//retrieve user primary account and saving account.
		PrimaryAccount primaryAccount = user.getPrimaryAccount();
		SavingsAccount savingsAccount = user.getSavingsAccount();

		model.addAttribute("primaryAccount", primaryAccount);
		model.addAttribute("savingsAccount", savingsAccount);

		return "userFront";
	}
}

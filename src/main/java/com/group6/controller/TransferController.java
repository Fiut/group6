package com.group6.controller;

import java.security.Principal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.group6.domain.PrimaryAccount;
import com.group6.domain.Recipient;
import com.group6.domain.SavingsAccount;
import com.group6.domain.User;
import com.group6.service.TransactionService;
import com.group6.service.UserService;

@Controller //registers this class as a controller and a bean in the spring container automatically.
@RequestMapping("/transfer")
public class TransferController {

	@Autowired
	private TransactionService transactionService;

	@Autowired
	private UserService userService;

	@RequestMapping(value = "/betweenAccounts", method = RequestMethod.GET) //when dealing with form, we need to define two things, the first being the GET method and the second is the post method
	public String betweenAccounts(Model model) { //for get method we must define a model and assigning three variables to it, each one with a default empty string.
		model.addAttribute("transferFrom", "");
		model.addAttribute("transferTo", "");
		model.addAttribute("amount", "");

		return "betweenAccounts";
	}

	@RequestMapping(value = "/betweenAccounts", method = RequestMethod.POST) //after form has been submitted to the post, we retrieve those variables with the @ModelAttribute annotation.
	public String betweenAccountsPost(@ModelAttribute("transferFrom") String transferFrom,
			@ModelAttribute("transferTo") String transferTo, @ModelAttribute("amount") String amount,
			Principal principal) throws Exception {
		User user = userService.findByUsername(principal.getName());
		PrimaryAccount primaryAccount = user.getPrimaryAccount();
		SavingsAccount savingsAccount = user.getSavingsAccount();
		transactionService.betweenAccountsTransfer(transferFrom, transferTo, amount, primaryAccount, savingsAccount);

		return "redirect:/userFront";
	}

	@RequestMapping(value = "/recipient", method = RequestMethod.GET)
	public String recipient(Model model, Principal principal) {
		//we first invoke the transactionservice to find whatever recipient list we have in the database.  
		List<Recipient> recipientList = transactionService.findRecipientList(principal);

		Recipient recipient = new Recipient(); //create a new recipient list.

		model.addAttribute("recipientList", recipientList); //attach the recipientList to the 'recipientList' variable. 
		model.addAttribute("recipient", recipient); //same as above.

		return "recipient";
	}

	@RequestMapping(value = "/recipient/save", method = RequestMethod.POST) //after the form has been posted, we are going to retrieve the recipient information. 
	public String recipientPost(@ModelAttribute("recipient") Recipient recipient, Principal principal) {

		User user = userService.findByUsername(principal.getName()); //first we are going to retrieve the users information, because the recipient is binded to the user.
		recipient.setUser(user); //set the recipient user to the current user who has logged in.. 
		transactionService.saveRecipient(recipient); //use transactionservice to save the new recipient.

		return "redirect:/transfer/recipient";
	}
	
	
	@RequestMapping(value = "/recipient/edit", method = RequestMethod.GET)
	//we assume that their will be a recipientname that will be passed through as a requestParamater, we will store the recipient name in a local string.
	public String recipientEdit(@RequestParam(value = "recipientName") String recipientName, Model model,
			Principal principal) {

		Recipient recipient = transactionService.findRecipientByName(recipientName); //first we retrieve the recipient from the transactionservice through findrecipientbyname.
		List<Recipient> recipientList = transactionService.findRecipientList(principal); //retrieve recipientlist.

		model.addAttribute("recipientList", recipientList); //binding
		model.addAttribute("recipient", recipient);

		return "recipient";
	}

	@RequestMapping(value = "/recipient/delete", method = RequestMethod.GET)
	@Transactional
	public String recipientDelete(@RequestParam(value = "recipientName") String recipientName, Model model,
			Principal principal) {

		transactionService.deleteRecipientByName(recipientName);

		List<Recipient> recipientList = transactionService.findRecipientList(principal);

		Recipient recipient = new Recipient();
		model.addAttribute("recipient", recipient);
		model.addAttribute("recipientList", recipientList);

		return "recipient";
	}

	@RequestMapping(value = "/toSomeoneElse", method = RequestMethod.GET)
	public String toSomeoneElse(Model model, Principal principal) {
		List<Recipient> recipientList = transactionService.findRecipientList(principal);

		model.addAttribute("recipientList", recipientList);
		model.addAttribute("accountType", "");

		return "toSomeoneElse";
	}

	@RequestMapping(value = "/toSomeoneElse", method = RequestMethod.POST)
	public String toSomeoneElsePost(@ModelAttribute("recipientName") String recipientName,
			@ModelAttribute("accountType") String accountType, @ModelAttribute("amount") String amount,
			Principal principal) {
		User user = userService.findByUsername(principal.getName());
		Recipient recipient = transactionService.findRecipientByName(recipientName);
		transactionService.toSomeoneElseTransfer(recipient, accountType, amount, user.getPrimaryAccount(),
				user.getSavingsAccount());

		return "redirect:/userFront";
	}
}

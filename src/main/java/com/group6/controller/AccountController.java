package com.group6.controller;

import java.security.Principal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.group6.domain.PrimaryAccount;
import com.group6.domain.PrimaryTransaction;
import com.group6.domain.SavingsAccount;
import com.group6.domain.SavingsTransaction;
import com.group6.domain.User;
import com.group6.service.AccountService;
import com.group6.service.TransactionService;
import com.group6.service.UserService;

@Controller //registers this class as a controller and a bean in the spring container automatically.
@RequestMapping("/account") //define requestmapping at the class level.
public class AccountController {

	@Autowired //wire up a user service
	private UserService userService;

	@Autowired
	private AccountService accountService;

	@Autowired
	private TransactionService transactionService;

	@RequestMapping("/primaryAccount") //request mapping points to the primary account.
	public String primaryAccount(Model model, Principal principal) { //we define a model and a principal
		List<PrimaryTransaction> primaryTransactionList = transactionService.findPrimaryTransactionList(principal.getName());

		User user = userService.findByUsername(principal.getName()); //we retrieve the user from the principal
		PrimaryAccount primaryAccount = user.getPrimaryAccount(); //we retrieve the primary account that is binded to the user. 

		model.addAttribute("primaryAccount", primaryAccount); //add primaryaccount to the attribute.
		model.addAttribute("primaryTransactionList", primaryTransactionList);

		return "primaryAccount"; //returns to the primaryaccount template page.
	}

	@RequestMapping("/savingsAccount")
	public String savingsAccount(Model model, Principal principal) {
		List<SavingsTransaction> savingsTransactionList = transactionService
				.findSavingsTransactionList(principal.getName());
		User user = userService.findByUsername(principal.getName());
		SavingsAccount savingsAccount = user.getSavingsAccount();

		model.addAttribute("savingsAccount", savingsAccount);
		model.addAttribute("savingsTransactionList", savingsTransactionList);

		return "savingsAccount";
	}

	@RequestMapping(value = "/deposit", method = RequestMethod.GET) //get method for /deposit which will be returning to the deposit.html page.
	public String deposit(Model model) { //two variables binded to the model, accountType and amount, both have empty values bind to it.
		model.addAttribute("accountType", "");
		model.addAttribute("amount", "");

		return "deposit";
	}

	@RequestMapping(value = "/deposit", method = RequestMethod.POST) //after we submit the form from the deposit template, we post to this method.
	public String depositPOST(@ModelAttribute("amount") String amount,
			@ModelAttribute("accountType") String accountType, Principal principal) {
		accountService.deposit(accountType, Double.parseDouble(amount), principal); //retrieve the amount variable by using the @ModelAttribute annotation and we assigned value to this. retrieve accountType and assign accountType string to it and define principal

		return "redirect:/userFront";
	}

	@RequestMapping(value = "/withdraw", method = RequestMethod.GET)
	public String withdraw(Model model) {
		model.addAttribute("accountType", "");
		model.addAttribute("amount", "");

		return "withdraw";
	}

	@RequestMapping(value = "/withdraw", method = RequestMethod.POST)
	public String withdrawPOST(@ModelAttribute("amount") String amount,
			@ModelAttribute("accountType") String accountType, Principal principal) {
		accountService.withdraw(accountType, Double.parseDouble(amount), principal);

		return "redirect:/userFront";
	}
}

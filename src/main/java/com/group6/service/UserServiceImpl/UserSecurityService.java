package com.group6.service.UserServiceImpl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.group6.dao.UserDAO;
import com.group6.domain.User;

@Service //this annotation is used to register the service bean.
public class UserSecurityService implements UserDetailsService {

	/** The application logger */
	private static final Logger LOG = LoggerFactory.getLogger(UserSecurityService.class);


	@Autowired
	private UserDAO userDao;

	
	@Override
	//we take a username as a input string, the userDao is used to try and find the username, if their username doesn't exist we throw an exception
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException { 
		User user = userDao.findByUsername(username); //if the user is not == to null, then we will directly return the user instance as a userDetails instance. this is used because in the in the securityConfiguration we are passing userdetails.
		if (user == null) {
			LOG.warn("Username {} not found", username);
			throw new UsernameNotFoundException("Username " + username + " not found");
		}
		return user;
	}
}
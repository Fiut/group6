package com.group6.service.UserServiceImpl;

import java.math.BigDecimal;
import java.security.Principal;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.group6.dao.PrimaryAccountDAO;
import com.group6.dao.SavingsAccountDAO;
import com.group6.domain.PrimaryAccount;
import com.group6.domain.PrimaryTransaction;
import com.group6.domain.SavingsAccount;
import com.group6.domain.SavingsTransaction;
import com.group6.domain.User;
import com.group6.service.AccountService;
import com.group6.service.TransactionService;
import com.group6.service.UserService;

@Service
public class AccountServiceImpl implements AccountService {

	private static int nextAccountNumber = 11223145; //random number used within accountgen method.

	@Autowired
	private PrimaryAccountDAO primaryAccountDao;

	@Autowired
	private SavingsAccountDAO savingsAccountDao;

	@Autowired
	private UserService userService;
	
	@Autowired
	private TransactionService transactionService;

	public PrimaryAccount createPrimaryAccount() { 
		PrimaryAccount primaryAccount = new PrimaryAccount(); //define a new instance
		primaryAccount.setAccountBalance(new BigDecimal(0.0)); //set account balance as the balance, as the accountbalance is a bigdecimal, we use a new method to create a new bigdecimal instance with the value of 0.0
		primaryAccount.setAccountNumber(accountGen()); //we set accountnumber, using the accountGen method.

		primaryAccountDao.save(primaryAccount); //save the primary account.
		
		//when we create a new primaryaccount instance we have not specified is primarykey, we use an automatic key generation. so only after we save the primary account, will the account id be generated and thus that is why we need the line below.
		return primaryAccountDao.findByAccountNumber(primaryAccount.getAccountNumber()); //
	}

	public SavingsAccount createSavingsAccount() {
		SavingsAccount savingsAccount = new SavingsAccount();
		savingsAccount.setAccountBalance(new BigDecimal(0.0));
		savingsAccount.setAccountNumber(accountGen());

		savingsAccountDao.save(savingsAccount);

		return savingsAccountDao.findByAccountNumber(savingsAccount.getAccountNumber()); 
	}
	
	public void deposit(String accountType, double amount, Principal principal) { //amount is of type double instead of BigDouble because we are just taking in a number and not really doing any operations on it.
        User user = userService.findByUsername(principal.getName()); //use principal to find username of person who has logged in, and then use userservice to find username and return instance.
        
        //need to check whether the deposit account is primary or saving.
        if (accountType.equalsIgnoreCase("Primary")) {
            PrimaryAccount primaryAccount = user.getPrimaryAccount(); //gets the primary account for the user.
            primaryAccount.setAccountBalance(primaryAccount.getAccountBalance().add(new BigDecimal(amount))); //retrieve the current account value and adds method of bigdeciaml class type, and within this we create a new bigdecimal based on the amount we are trying to deposit.
            primaryAccountDao.save(primaryAccount);

            Date date = new Date();

            PrimaryTransaction primaryTransaction = new PrimaryTransaction(date, "Deposit to Primary Account", "Account", "Finished", amount, primaryAccount.getAccountBalance(), primaryAccount);
            transactionService.savePrimaryDepositTransaction(primaryTransaction);
            
        } else if (accountType.equalsIgnoreCase("Savings")) {
            SavingsAccount savingsAccount = user.getSavingsAccount();
            savingsAccount.setAccountBalance(savingsAccount.getAccountBalance().add(new BigDecimal(amount)));
            savingsAccountDao.save(savingsAccount);

            Date date = new Date();
            SavingsTransaction savingsTransaction = new SavingsTransaction(date, "Deposit to savings Account", "Account", "Finished", amount, savingsAccount.getAccountBalance(), savingsAccount);
            transactionService.saveSavingsDepositTransaction(savingsTransaction);
        }
    }
    
    public void withdraw(String accountType, double amount, Principal principal) {
        User user = userService.findByUsername(principal.getName());

        if (accountType.equalsIgnoreCase("Primary")) {
            PrimaryAccount primaryAccount = user.getPrimaryAccount();
            primaryAccount.setAccountBalance(primaryAccount.getAccountBalance().subtract(new BigDecimal(amount)));
            primaryAccountDao.save(primaryAccount);

            Date date = new Date();

            PrimaryTransaction primaryTransaction = new PrimaryTransaction(date, "Withdraw from Primary Account", "Account", "Finished", amount, primaryAccount.getAccountBalance(), primaryAccount);
            transactionService.savePrimaryWithdrawTransaction(primaryTransaction);
        } else if (accountType.equalsIgnoreCase("Savings")) {
            SavingsAccount savingsAccount = user.getSavingsAccount();
            savingsAccount.setAccountBalance(savingsAccount.getAccountBalance().subtract(new BigDecimal(amount)));
            savingsAccountDao.save(savingsAccount);

            Date date = new Date();
            SavingsTransaction savingsTransaction = new SavingsTransaction(date, "Withdraw from savings Account", "Account", "Finished", amount, savingsAccount.getAccountBalance(), savingsAccount);
            transactionService.saveSavingsWithdrawTransaction(savingsTransaction);
        }
    }

	private int accountGen() { //returns the nextaccountnumber, every time we do an automatic increase by 1.
		return ++nextAccountNumber;
	}

}

package com.group6.service.UserServiceImpl;


import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.group6.dao.RoleDAO;
import com.group6.dao.UserDAO;
import com.group6.domain.User;
import com.group6.domain.security.UserRole;
import com.group6.service.AccountService;
import com.group6.service.UserService;


//This class implements the empty methods that have been made in UserService.java.
@Service //Registered as a service bean,
@Transactional //use at method level or class level.
public class UserServiceImpl implements UserService {

	private static final Logger LOG = LoggerFactory.getLogger(UserService.class);
	
	@Autowired //this annotation is based on the idea of dependency injection. 
	private UserDAO userDao;
	
	@Autowired
	private RoleDAO roleDao;
	
	@Autowired
	private AccountService accountService;
	
	@Autowired
	private BCryptPasswordEncoder passwordEncoder;

	public void save(User user) {
		userDao.save(user);
	}

	public User findByUsername(String username) {
		return userDao.findByUsername(username);
	}

	public User findByEmail(String email) {
		return userDao.findByEmail(email);
	}

	public User createUser(User user, Set<UserRole> userRoles) { //takes two parameters.
		User localUser = userDao.findByUsername(user.getUsername()); //first checks to see if the user exists, by envoking the find by username method.

		if (localUser != null) {
			LOG.info("User with username {} already exist. Nothing will be done. ", user.getUsername()); //if the user does exist we will log the information stating that nothing will be done.
		} else { //otherwise if the user doesnt exist we will prepare to store the user into our database.
			String encryptedPassword = passwordEncoder.encode(user.getPassword()); //first we will encrypt the password.
			user.setPassword(encryptedPassword); //the encrypted password is set to the user password.

			for (UserRole ur : userRoles) { //look through each of the user roles we have assigned to this user and we will persist those roles into our database. 
				roleDao.save(ur.getRole());
			}
 
			user.getUserRoles().addAll(userRoles); //add the userRoles that we have to our users.

			//we will define the primaryAccount and savingsAccount by evoking the accountService.create....
			user.setPrimaryAccount(accountService.createPrimaryAccount());
			user.setSavingsAccount(accountService.createSavingsAccount());

			localUser = userDao.save(user); //the user os saved
		}

		return localUser; // user is returned.
	}

	public boolean checkUserExists(String username, String email) { //checks whether the user exists
		if (checkUsernameExists(username) || checkEmailExists(username)) {
			return true;
		} else {
			return false;
		}
	}

	public boolean checkUsernameExists(String username) { //checks whether the username exists
		if (null != findByUsername(username)) {
			return true;
		}

		return false;
	}

	public boolean checkEmailExists(String email) { //checks whether the email exists, returns boolean value True if it does.
		if (null != findByEmail(email)) {
			return true;
		}

		return false;
	}

	public User saveUser(User user) {
		return userDao.save(user);
	}
	
	
}

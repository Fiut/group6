package com.group6.service;

import java.security.Principal;

import com.group6.domain.PrimaryAccount;
import com.group6.domain.SavingsAccount;


public interface AccountService {
	// methods for the account service 
	PrimaryAccount createPrimaryAccount();
    SavingsAccount createSavingsAccount();
    void deposit(String accountType, double amount, Principal principal);
    void withdraw(String accountType, double amount, Principal principal);
    
    
}

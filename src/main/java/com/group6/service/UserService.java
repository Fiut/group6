package com.group6.service;


import java.util.Set;

import com.group6.domain.User;
import com.group6.domain.security.UserRole;

public interface UserService {
	//defines the methods that will be implemented in the userServiceImplementation class.
	User findByUsername(String username);

    User findByEmail(String email);

    boolean checkUserExists(String username, String email);

    boolean checkUsernameExists(String username);

    boolean checkEmailExists(String email);
    
    void save (User user);
    
    User createUser(User user, Set<UserRole> userRoles);
    
    User saveUser (User user); 
    
}
